
import crypto from 'crypto'

const randomValueHex = len => {
  return crypto
    .randomBytes(Math.ceil(len / 2))
    .toString('hex') // convert to hexadecimal format
    .slice(0, len) // return required number of characters
}

const displayConf = () => {
  console.log('************************************************************************************')
  console.log('The following configuration settings are used:')
  console.log(`Source directory for Markdown files: ${process.env.npm_config_sourceDir}`)
  console.log(`Aggregate JSON in one file?: ${process.env.npm_config_aggregate}`)
  console.log(`Output directory for JSON file(s): ${process.env.npm_config_outputDir}`)
  console.log(`Output file for aggregated JSON: ${process.env.npm_config_outputFile}`)
  console.log(`Input file: ${process.env.npm_config_inputFile}`)
  console.log('************************************************************************************')
}

module.exports = {
  randomValueHex, displayConf
}
