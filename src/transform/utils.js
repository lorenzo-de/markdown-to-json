import _ from 'lodash'
import he from 'he'
import yamlJs from 'yamljs'
import isUuid from 'is-uuid'

const heHtmlEscaper = (html) => {
  if (html) return he.escape(html)
}

const yamlJsParser = (yamlString) => {
  return new Promise((resolve, reject) => {
    if (!yamlString) reject(new Error('yaml string cannot be empty.'))
    resolve((yamlJs.parse(yamlString)))
  })
}

const isUuidChecker = (uuid) => {
  return isUuid.anyNonNil(uuid)
}

const removeYaml = (str) => {
  /* Regex:
   * find everything between --- and ---.
   * \s\S is a DOTALL workaround because .* would match every character except newlines.
   */
  return _.replace(str.toString(), /(-{3})([\s\S]*)(-{3})/, '')
}

module.exports = {
  heHtmlEscaper,
  yamlJsParser,
  isUuidChecker,
  removeYaml
}
