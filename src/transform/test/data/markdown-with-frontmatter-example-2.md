---
title: "Digital Rights Foundation - 2017 in review "
description: 'This report compiles the brief overview of activities of the Digital Rights Foundation during the year 2017'
slug: ""
summary: ""
teaserImage: "/media/digitalrightsreport.png"
contentType: ""
projects: "Gender and Tech"
collection: ""
tags:
  - privacy
  - surveillance
  - "free spech"
  - advocacy
  - "digital security"
  - "online harassment"
uuid: b594e594-05ce-11e8-ba89-0ed5f89f718b
originPublication: "Digital Rights Foundation"
originLink: "https://digitalrightsfoundation.pk/wp-content/uploads/2018/01/Year-in-Review-2017.pdf"
date: "01.01.2018 00:00:00"
published: true
locale: "en"
translations: ""
author: ""
related: ""
---
[This report](https://digitalrightsfoundation.pk/wp-content/uploads/2018/01/Year-in-Review-2017.pdf) compiles the brief overview of activities of the Digital Rights Foundation during the year 2017. Digital Rights Foundation (DRF) is a non-government non-profit organization registered legally in Pakistan in 2013. The report describes how the Pakistan's first Cyber Harassment Helpline worked during 2017. This helpline was launched in December 2016 to address the prevailing forms of online harassment, especially gendered harassment, in online spaces in Pakistan. The helpline has received an overwhelming response from the masses, indicating that online harassment in particular and gender based violence in general is extremely pervasive, and needs to be addressed urgently

The Helpline received 1,551 complaints in the form of calls, emails, and Facebook messages between December 1, 2016 till November 20, 2017, out of which 1,476 are cases received over calls. Out of the calls received at the helpline, 67% identified as women, and 33% of the callers were men.
