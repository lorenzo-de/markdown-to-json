---
title: "Interview with Women's Media Collective, Sri Lanka: About lesbian tutorials and other strategies"
description: 'Shubha Kayastha interviewed research team including Sanchia Brown, Programme Officer at Women and Media Collective (WMC) Sri Lanka and Coordinator of EROTICS, Sri Lanka'
slug: 'interview-womens-media-collective-sri-lanka-about-lesbian-tutorials-and-other-strategies'
summary: 'Shubha Kayastha interviewed research team including Sanchia Brown, Programme Officer at Women and Media Collective (WMC) Sri Lanka and Coordinator of EROTICS, Sri Lanka; Subha Wijesiriwardena, a writer and a blogger working on gender and sexuality project in WMC and also associated with Internews in a project which supports Freedom House which looks like strengthening local LGBTIQ (Lesbian, Gay, Bisexual, Transgender, Intersex and Queer) groups in Sri Lanka; and Paba Deshapriya who has been working at The Grassroot Trust around the issue of sexual reproductive health and rights,'
teaserImage: 'globalsurvey.png'
contentType: long-essay
projects:
  - Gender and Tech
collection:
  - Gender and tech
  - security
tags:
  - gender
  - discrimination
  - lesbian
  - tutorial
uuid: 4cd39a58-6bcc-4704-a9ce-1dee54282280
originPublication: "GenderIT"
originLink: "https://www.genderit.org/articles/interview-womens-media-collective-sri-lanka-about-lesbian-tutorials-and-other-strategies"
date: "05-Feb-2015 17:02:00"
published: true
locale: en
translations:
  - b8157885-a3f8-4e92-bfe4-6a0373329a7c
authors: b8157885-a3f8-4e92-bfe4-6a0373329a7c
related:
  - b87d20cc-37b3-4945-91dc-3b77ddd8dd1c
  - 83453197-dac1-4492-a5c1-f2b96e93f2b5
---
