
import _ from 'lodash'
import assert from 'assert'
import toVfile from 'to-vfile'
import isHtml from 'is-html'
import yamlLint from 'yaml-lint'
import isJson from 'is-json'

import { frontmatterFromFile, yamlToJsObject, markdownToHtml, mergeFrontmatterAndContentJson } from '../lib'
import { yamlJsParser, removeYaml } from '../utils'
import { remarkMarkdownToHtml, remarkFrontmatterFromFile } from '../remark/remarkProcessors'

describe('content transformations', () => {
  describe('#markdownToHtml()', () => {
    it('should output HTML', (done) => {
      const file = toVfile.readSync('./src/transform/test/data/markdown-examples.md')
      const html = markdownToHtml(file, remarkMarkdownToHtml)
      Promise.resolve(html)
        .then(res => assert.ok(isHtml(res)))
        .catch(e => e)
        .finally(done)
    })
  })

  describe('#frontmatterFromFile()', () => {
    it('should output frontmatter ', (done) => {
      const file = toVfile.readSync('./src/transform/test/data/frontmatter-example.md')
      const frontmatter = frontmatterFromFile(file, remarkFrontmatterFromFile)
      Promise.resolve(frontmatter)
        .then(res => yamlLint.lint(res))
        .then(() => {
          assert.ok(true)
        }).catch(error => {
          console.log(error)
          assert.ok(false)
        })
        .finally(done)
    })
  })

  describe('#yamlToJsObject()', () => {
    it('should output JS object', (done) => {
      const file = toVfile.readSync('./src/transform/test/data/frontmatter-example.md')
      const frontmatter = frontmatterFromFile(file, remarkFrontmatterFromFile)
      const obj = yamlToJsObject(frontmatter, yamlJsParser)
      Promise.resolve(obj)
        .then(res => assert.ok(_.isObject(res)))
        .catch(e => e)
        .finally(done)
    })
  })

  describe('#mergeFrontmatterAndContentJson()', () => {
    it('should return JSON', (done) => {
      const fm = toVfile.readSync('./src/transform/test/data/frontmatter-example.md')
      const md = toVfile.readSync('./src/transform/test/data/markdown-examples.md')
      const frontmatter = frontmatterFromFile(fm, remarkFrontmatterFromFile)
      const html = markdownToHtml(md, remarkMarkdownToHtml)
      const json = mergeFrontmatterAndContentJson(frontmatter, html, process.env.npm_config_contentKey)
      Promise.resolve(json)
        .then(res => {
          if (res) {
            assert.ok(assert.ok(isJson(res)))
          } else {
            assert.ok(assert.ok(false))
          }
        })
        .catch(e => e)
        .finally(done)
    })
  })

  describe('#mergeFrontmatterAndContentJson()', () => {
    it('there should be a content key in JSON', (done) => {
      const fm = toVfile.readSync('./src/transform/test/data/frontmatter-example.md')
      const md = toVfile.readSync('./src/transform/test/data/markdown-examples.md')
      const key = process.env.npm_config_contentKey
      const frontmatter = frontmatterFromFile(fm, remarkFrontmatterFromFile)
      const html = markdownToHtml(md, remarkMarkdownToHtml)
      const json = mergeFrontmatterAndContentJson(frontmatter, html, key)
      Promise.resolve(json)
        .then(res => assert.ok(_.has(json, key)))
        .catch(e => e)
        .finally(done)
    })
  })
})

describe('utility functions', () => {
  describe('#removeYaml()', () => {
    it('should remove words, digits and special characters between --- delimiter', (done) => {
      let test = false
      if (removeYaml('---FRONTMATTER Frontmatter 432141 3z2&7  27 "787" zz---').length > 0) {
        test = false
      } else {
        test = true
      }
      assert.ok(test)
      done()
    })
  })

  describe('#removeYaml()', () => {
    it('should remove newline between --- delimiter', (done) => {
      let test = false
      if (removeYaml('---\n---').length > 0) {
        test = false
      } else {
        test = true
      }
      assert.ok(test)
      done()
    })
  })

  describe('#removeYaml()', () => {
    it('should remove newline between any character and delimited by ---', (done) => {
      let test = false
      if (removeYaml('---Fr0ntm-tter\nFrÖntm$tter---').length > 0) {
        test = false
      } else {
        test = true
      }
      assert.ok(test)
      done()
    })
  })

  describe('#removeYaml()', () => {
    it('should not return string with frontmatter keys title or uuid', (done) => {
      const md = toVfile.readSync('./src/transform/test/data/markdown-with-frontmatter-example-2.md')
      const str = removeYaml(md.contents.toString())
      let test = false
      if (/title:|uuid:/.test(str)) {
        test = false
      } else {
        test = true
      }
      assert.ok(test)
      done()
    })
  })
})
