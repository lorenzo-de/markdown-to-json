
import _ from 'lodash'
import { remarkMarkdown, remarkMarkdownToHtml, remarkFrontmatterFromFile } from './remark/remarkProcessors'
import { heHtmlEscaper, yamlJsParser, isUuidChecker, removeYaml } from './utils'
import { frontmatterFromFile, yamlToJsObject, markdown, markdownToHtml, escapeHtml, idChecker, mergeFrontmatterAndContentJson } from './lib'

/* Transform virtual file with frontmatter and markdown to JSON object
 * @param {vfile} file - a virtual file object
 * @returns {promise} promise - promise which resolves to JSON object
 */
const virtualFileToJsonConverter = (file) => {
  return new Promise((resolve, reject) => {
    // Get YAML and Markdown content
    frontmatterFromFile(file, remarkFrontmatterFromFile)
      .then(yaml => {
        yamlToJsObject(yaml, yamlJsParser)
          .then(fm => {
            // Return content as HTML
            if (process.env.npm_config_convertToHtml) {
              markdownToHtml(file, remarkMarkdownToHtml).then(html => {
                if (idChecker(fm.uuid, isUuidChecker)) {
                  // Escape HTML
                  if (process.env.npm_config_escapeHtml) {
                    html = escapeHtml(html, heHtmlEscaper)
                  }
                  resolve(mergeFrontmatterAndContentJson(fm, html, process.env.npm_config_contentKey))
                } else reject(new Error('No or malformed UUID for ' + file.basename))
              }).catch(e => console.log(e))
            // Return content as Markdown
            } else {
              markdown(removeYaml(file.contents), remarkMarkdown).then(markdown => {
                if (idChecker(fm.uuid, isUuidChecker)) {
                  resolve(mergeFrontmatterAndContentJson(fm, markdown, process.env.npm_config_contentKey))
                } else reject(new Error('No or malformed UUID for ' + file.basename))
              }).catch(e => console.log(e))
            }
          }).catch(e => console.log(e))
      }).catch(e => console.log(e))
  })
}

/* Batch transform virtual files to JSON files
 * @param {vfiles} files - array of virtual file objects
 * @param {function} transformerFunction - the transformer function to apply to the files
 * @returns {promise} array - array of promises each resolving to JSON object
 */
const virtualFilesToJson = (files, transformerFunction) =>
  _.map(files, file => {
    return transformerFunction(file)
  })

const virtualFileToJson = (file, transformerFunction) => {
  return transformerFunction(file)
}

/* Resolve array of promises into JSON objects and aggregate into one JSON object
 * @param {array} jsonObjectPromises - array with promises resolving to JSON objects
 * @returns {string} allJson - JSON string
 */
const aggregateJson = async (jsonObjectPromises) => {
  let obj = {}
  const allJson = await Promise.all(jsonObjectPromises)
    .then(
      jsonObjectPromises.map(
        promise => promise.then(
          data => {
            obj = Object.assign(obj, data)
          }
        ).catch(e => console.log(e))
      )
    ).catch(e => console.log(e))
  return JSON.stringify(allJson)
}

const resolveJson = async (jsonObj) => {
  const json = await Promise.resolve(jsonObj)
  return (JSON.stringify(json))
}

module.exports = {
  virtualFilesToJson, virtualFileToJson, virtualFileToJsonConverter, aggregateJson, resolveJson
}
