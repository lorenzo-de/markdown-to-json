import remark from 'remark'
import remarkmarkdown from 'remark-parse'
import remarkhtml from 'remark-html'
import remarkfrontmatter from 'remark-frontmatter'
import remarkYamlToData from './remark-plugins/yamlToData'

const remarkMarkdown = (file) => {
  return remark()
    .use(remarkmarkdown)
    .process(file)
    .then(file => {
      return String(file)
    }, err => {
      console.log(err)
    })
}

const remarkMarkdownToHtml = (file) => {
  return remark()
    // This is removing frontmatter from contents only in combination of these two remark plugins but why?
    .use(remarkfrontmatter)
    .use(remarkhtml)
    .process(file)
    .then(file => {
      return String(file)
    }, err => {
      console.log(err)
    })
}

/* Get frontmatter from file
 * @param {vfile} file - a virtual file with markdown
 * @returns {promise} promise - frontmatter from vfile.data
 */
const remarkFrontmatterFromFile = (file, frontmatterPlugin = remarkfrontmatter, yamlWriterPlugin = remarkYamlToData) => {
  const node = remark()
    .use(frontmatterPlugin)
    .use(yamlWriterPlugin)
    .process(file)
  return node.then(res => res.data)
}

module.exports = {
  remarkMarkdown,
  remarkMarkdownToHtml,
  remarkFrontmatterFromFile
}
