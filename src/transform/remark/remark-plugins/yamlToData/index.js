import visit from 'unist-util-visit'

// Custom remark plugin to write YAML to the data object in a vfile

// Unified attacher:
// https://github.com/unifiedjs/unified#function-attacheroptions
const yamlToData = () => {
  return transformer
}

// Saves YAML frontmatter as data in vfile.
// I couldnt write to vfile.contents, using vfile.data instead.
// Unified transformer:
// https://github.com/unifiedjs/unified#function-transformernode-file-next
const transformer = (node, file) => {
  const visitor = node => {
    file.data = node.value
  }
  visit(node, 'yaml', visitor)
}

module.exports = yamlToData
