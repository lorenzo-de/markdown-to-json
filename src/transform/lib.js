import _ from 'lodash'

/* Return markdown from virtual file with markdown processor
 * @param {vfile} vFile - a virtual file with markdown
 * @param {processor} markdownProcessor - markdown processor
 * @returns {promise} promise - processed file as markdown string
 */
const markdown = (vFile, markdownProcessor) => {
  if (vFile && markdownProcessor) {
    return markdownProcessor(vFile)
  }
}

/* Transform Markdown in virtual file to HTML with markdown processor
 * @param {vfile} vFile - a virtual file with markdown
 * @param {processor} markdownToHtmlProcessor - markdown to HTML processor
 * @returns {promise} promise - processed file as HTML string
 */
const markdownToHtml = (vFile, markdownToHtmlProcessor) => {
  if (vFile && markdownToHtmlProcessor) {
    return markdownToHtmlProcessor(vFile)
  }
}

/* Escape HTML
 * @param {string} html - HTML string
 * @param {processor} htmlEscaper - the processor
 * @returns {string} string - escaped HTML string
 */
const escapeHtml = (html, htmlEscaper) => {
  if (html && htmlEscaper) return htmlEscaper(html)
}

/* Get frontmatter from file
 * @param {vfile} vFile - a virtual file
 * @param {processor} processor - a processor to extract frontmatter
 * @returns {promise} promise - frontmatter from vfile.data
 */
const frontmatterFromFile = (vFile, processor) => {
  if (vFile && processor) return processor(vFile)
}

/* Transform YAML string to JS object
 * @param {string} yamlString - YAML string
 * @param {parser} yamlParser - YAML parser
 * @returns {promise} promise - JS object
 */
const yamlToJsObject = (yamlString, yamlParser) => {
  if (yamlString && yamlParser) return yamlParser(yamlString)
}

/* Check if ID is valid
 * @param {string} id - the ID to check
 * @param {checker function} idCheckerMethod - the function which checks the ID
 * @returns {boolean} boolean - returns true if ID is tested successfully by the checker function
 */
const idChecker = (id, idCheckerMethod) => {
  return idCheckerMethod(id)
}

/* Merge frontmatter JSON and content JSON to one JSON blob
 * @param {object} frontmatter - frontmatter JSON
 * @param {object} content - content JSON
 * @param {string} contentKey - sets the key for content
 * @returns {object} object - returns object with frontmatter and content
 */
const mergeFrontmatterAndContentJson = (frontmatter, content, contentKey) => {
  const json = frontmatter
  _.merge(
    json, _.set(
      {}, contentKey, content
    )
  )
  return json
}

module.exports = {
  escapeHtml, markdown, markdownToHtml, frontmatterFromFile, yamlToJsObject, idChecker, mergeFrontmatterAndContentJson
}
