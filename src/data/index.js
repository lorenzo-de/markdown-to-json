
import walk from 'walk'
import path from 'path'
import fs from 'fs'
import toVfile from 'to-vfile'

const readVFileSync = (path) =>
  toVfile.readSync(path)

/* Read files in directory recursively and output array of filesAsVFilesArray
 * @params {string} directory - root directory
 * @returns {promise} files - array of filesAsVFilesArray
 */
const filesAsVFilesArray = directory => {
  return new Promise((resolve, reject) => {
    const files = []
    const options = {
      followLinks: false
    }
    const walker = walk.walk(directory, options)

    walker.on('file', (root, fileStats, next) => {
      fs.readFile(fileStats.name, () => {
        files.push(readVFileSync(path.resolve(root, fileStats.name)))
        next()
      })
    })

    walker.on('errors', (root, nodeStatsArray, next) => {
      next()
    })

    walker.on('end', () => {
      console.log('Done walking trough files in directory.')
      console.log(files)
      resolve(files)
    })
  })
}

const fileAsVFile = path => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) reject(new Error(err))
      else {
        resolve(readVFileSync(path))
      }
    })
  })
}

const writeFile = (content, file) => {
  const wStream = fs.createWriteStream(file)
  wStream.write(content)
  wStream.end()
}

module.exports = {
  filesAsVFilesArray, fileAsVFile, writeFile
}
