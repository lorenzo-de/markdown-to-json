
## Description

Takes files with Markdown and (YAML) frontmatter and transforms them to JSON. Can optionally transform to HTML first.

### Requirements

Tested with Node v11.2.0.

### Install

```npm install```

### Run

Important:

Make sure the output directory exists otherwise the script fails! For the examples in this README, run the command `mkdir _data` first.
Sample content is in the `content` directory in this repository.

Transform one Markdown file to JSON:

```npm run start --sourceDir=content --outputDir=_data --inputFile=my-markdown-file.md --contentKey=contents --convertToHtml=true --escapeHtml=true```

Transform all Markdown files in a directory recursively and aggregate JSON in one output file:

```npm run start --aggregate=true --sourceDir=content --outputFile=data.json --outputDir=_data --contentKey=contents --convertToHtml=true --escapeHtml=true```

### Run options

- `sourceDir`: String - directory with Markdown files
- `outputDir`: String - output directory
- `inputFile`: String - when cherry picking a single Markdown file to be transformed
- `outputFile`: String - the resulting JSON file when walking an input directory with Markdown files
- `contentKey`: String - the JSON key which holds the body text
- `convertToHtml`: Boolean - if true, transforms Markdown to HTML
- `escapeHtml`: Boolean - if true, escapes HTML

### Tests

Run unit tests:

```npm run test```

### Under the hood

Uses [Remark](https://www.npmjs.com/package/remark) plugins to transform Markdown and YAML in [vFiles](https://www.npmjs.com/package/vfile) to JSON.
