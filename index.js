
import path from 'path'

import { virtualFileToJsonConverter, virtualFilesToJson, virtualFileToJson, aggregateJson, resolveJson } from './src/transform'
import { filesAsVFilesArray, fileAsVFile, writeFile } from './src/data'
import { displayConf, randomValueHex } from './src/utils'

displayConf()

// Read all files in directory and aggregate JSON output in one file
if (process.env.npm_config_aggregate === 'true') {
  if (process.env.npm_config_sourceDir && process.env.npm_config_outputDir && process.env.npm_config_outputFile) {
    const file = path.resolve(process.env.npm_config_outputDir, process.env.npm_config_outputFile)
    console.log(`Aggregate flag is set. Aggregating all files from ${process.env.npm_config_sourceDir} and ignoring other settings.`)
    filesAsVFilesArray(process.env.npm_config_sourceDir)
      .then(files => aggregateJson(virtualFilesToJson(files, virtualFileToJsonConverter))
        .then(json => {
          writeFile(json, file)
          console.log('Output written to file: ' + file)
        })
      )
  } else (console.log('Could not return aggregated JSON because one or more configuration settings were missing.'))
// Read one file and output JSON to file
} else {
  if (process.env.npm_config_sourceDir && process.env.npm_config_outputDir) {
    const outputFile = path.resolve(process.env.npm_config_outputDir, randomValueHex(12) + '.json')
    const inputFile = path.resolve(process.env.npm_config_sourceDir, process.env.npm_config_inputFile)
    console.log(`Processing file ${inputFile}`)
    fileAsVFile(inputFile)
      .then(file => resolveJson(virtualFileToJson(file, virtualFileToJsonConverter))
        .then(json => {
          writeFile(json, outputFile)
          console.log('Output written to file: ' + outputFile)
        })
      )
  } else (console.log('Could not return JSON because one or more configuration settings were missing.'))
}
